export default class PathService {

    constructor($window, $q, Restangular) {
        this.Restangular = Restangular
        this.$q = $q
        this.$window = $window

        this._pathService = this.Restangular.service('paths');
        
    }
    
    /**
     * Get list of all paths
     *
     * @returns {Object} paths
     */
    getPaths() {
        this._pathService.getList().then(paths => {
            return paths
        });
    }
    
    /**
     * Get a path
     *
     * @param {Integer} path_id 
     * @returns {Object} path
     */
    getPath(path_id) {
        return this._pathService.get(path_id);
    }

}

PathService.$inject = ['$window', '$q', 'Restangular'];
