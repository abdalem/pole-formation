export default class AuthService {

    constructor($window, $q, Restangular) {
        this.Restangular = Restangular
        this.$q = $q
        this.$window = $window

        this._authorize = this.Restangular.one('oauth2/authorize');
        this._token = this.Restangular.service('oauth2/token');
//        this._tokenRequestCredentials = {
//            'grant_type': 'authorization_code',
//            'client_id': 'groupeavantages',
//            'redirect_uri': '/',
//            'code': 'KKhipLODkQ4e9tmMW2ftsEZHIg1vnvid6PZIy3vLHYTIK50OSs9YOSkehCd592fJfpPyuNiDSrNJ9RxuVsWawCQNZCyZjTAofLq5GqvJm9wLipSKSEsyHLUIaDlPwEa0gsu8o+UDNJ79+QkLmn6WWRqI8g3fEkLDvADcZc/lcaFwIfzJGRHm55jS1HbB/XMq/VQUXupOYnRoFO/6SG1FkhhfmtLX6PAEpz8u4YchYqqHh5PzBIBqYPO7Q0K2dIF6s178idnQykpw2i4UwG7QwzSEnFnezthvaMOSkm0vOgfjR77Actdrnt2K/LuMyoX5Mi4wcAzmgUhmgjDAHXiXQmDwx8GfjeIers0M1w566f7x3XarI3OB99UAz8qEggltMrVHoM0hCq9r2xa8EkNIFdwcA4NITLJnVwK54sSkKvv95w5aTTnd+N2TiO1e+6hRy+B7qx9Y4w1WfvFk/OJ4HSjLqDaaDWnXSVgyFsoCMq49KgBU/9V/QMzeeICGMRZQ'
//        };

        this._tokenRequestCredentials = {
            'grant_type': 'client_credentials',
            'scope': 'learning_content'};

        this.credentials = {};
    }
    /**
     * Request for code authorization
     * 
     * @param {String} response_type
     * @param {String} client_id
     * @param {String} redirect_uri
     * @param {String} scope 
     * @param {String} state 
     */
    authorizationRequest(client_id, redirect_uri, scopes, state) {
//        this._authorize.get({
//            'response_type': 'code',
//            'client_id': 'groupeavantages',
//            'redirect_uri': 'http://localhost:8080/#!/connexion',
//            'scope': 'user_email',
//            'state': 'cvsg6c3r4rmabbr9mho9',
//        }).then(authorize => {
//            return authorize
//        })
    }

    /**
     * Request for token
     *
     * @param {String} login 
     * @param {String} password 
     * @returns {Object} credentials
     */
    tokenRequest(login, password) {
        let headers = {
            Authorization: this.baseAuth(login, password)
        }
        return this._token.post(this._tokenRequestCredentials, {}, headers)
                .then(credentials => {
                    this.credentials = credentials.originalElement
                    for (var credential in this.credentials) {
                        this.$window.localStorage.setItem(credential, this.credentials[credential])
                    }
                });
    }

    /**
     * Create the basic auth hash
     * 
     * @param {String} user 
     * @param {String} pass 
     * 
     * @returns {String} hash
     */
    baseAuth(user, pass) {
        let basic = user + ':' + pass
        let hash = btoa(basic)

        return "Basic " + hash
    }

}

AuthService.$inject = ['$window', '$q', 'Restangular'];
