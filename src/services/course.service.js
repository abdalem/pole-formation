export default class CourseService {

    constructor($window, $q, Restangular) {
        this.Restangular = Restangular
        this.$q = $q
        this.$window = $window

        this._courseService = Restangular.service('courses');

    }

    /**
     * Get result from a query search on courses
     *
     * @param {string} query 
     * @returns {Object} courses
     */
    searchCourses(query) {
        if (query) {
            this._courseService.getList({'q': query}).then(courses => {
                return courses
            })
        }
        this._courseService.getList().then(courses => {
            return courses
        })
    }

    /**
     * Get a course
     *
     * @param {integer} course_id 
     * @returns {Object} course
     */
    getCourse(course_id) {
        return this._courseService.get(course_id);
    }

    /**
     * Get a course's table of content
     *
     * @param {integer} course_id 
     * @returns {Object} tableOfContent
     */
    getTableOfContent(course_id) {
        return this._courseService.one(course_id).one('table-of-content').get();
    }

    /**
     * Get a course's introduction
     *
     * @param {integer} course_id 
     * @returns {Object} introduction
     */
    getIntroduction(course_id) {
        return this._courseService.one(course_id).one('introduction').get();
    }

}

CourseService.$inject = ['$window', '$q', 'Restangular'];
