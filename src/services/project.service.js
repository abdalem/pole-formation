export default class ProjectService {

    constructor($window, $q, Restangular) {
        this.Restangular = Restangular
        this.$q = $q
        this.$window = $window

        this._projectService = Restangular.service('projects');

    }

    /**
     * Get skills list of a project
     *
     * @param {Integer} project_id 
     * @returns {Object} skills
     */
    getProjectsSkills(project_id) {
        this._projectService.one(project_id).all('skills').getList().then(skills => {
            return skills
        })
    }

}

ProjectService.$inject = ['$window', '$q', 'Restangular'];
