export default class UserService {

    constructor($window, $q, $rootScope, Restangular, AuthService) {
        this.Restangular = Restangular
        this.$q = $q
        this.$window = $window
        this.$rootScope = $rootScope
        this.AuthService = AuthService

        this._currentUser = this.Restangular.one('me');
        this._userService = this.Restangular.service('users');
        this._user = {}; // cached user

    }

    /**
     * Get actual connected user
     *
     * @returns {Object} user
     */
    getCurrentUser() {
        if (Object.keys(this._user).length !== 0) {
            return this.$q.resolve(this._user)
        }
        return this._currentUser.get()
                .then(user => this._user = user);
    }
    
    /**
     * Get list of all courses followed by current user
     *
     * @param {Integer} user_id 
     * @returns {Object} followedCourses
     */
    getFollowedCourses(user_id) {
        this._userService.one(user_id).all('followed-courses').getList().then(followedCourses => {
            return followedCourses
        });
    }

    /**
     * Get list of all courses for this current user
     *
     * @param {Integer} user_id
     * @param {Integer} course_id
     * @returns {Object} courses
     */
    getCourses(user_id, course_id) {
        return this._userService.one(user_id).all('courses').get(course_id);
    }

    /**
     * Follow a course
     *
     * @param {Integer} user_id
     * @param {Integer} course_id
     * @returns 201
     */
    followCourse(user_id, course_id) {
        return this._userService.one(user_id).all('courses').one(course_id).one('follow').post();
    }

    /**
     * Get the table of content of a course for this user
     *
     * @param {Integer} user_id
     * @param {Integer} course_id
     * @returns {Object} tableOfContent
     */
    tableOfContent(user_id, course_id) {
        return this._userService.one(user_id).all('courses').one(course_id).one('table-of-content').get();

    }

}

UserService.$inject = ['$window', '$q','$rootScope' , 'Restangular', 'AuthService'];
