import searchComponent from './search.component'

export default angular.module('pf.search', [])
  .component('search', searchComponent)