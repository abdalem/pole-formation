import template from './search.html'
import controller from './search.controller'

export default {
  template,
  controller,
  bindings: {
    onCancel: '&'
  }
}
