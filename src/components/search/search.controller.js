export default class SearchController {
  constructor(CourseService, $window, $timeout) {
    this.CourseService = CourseService
    this.$window = $window
    this.$timeout = $timeout
  }

  $onInit() {
    this.$timeout(() => {
      this.$window.document.getElementById('search').focus()
    })
  }

  searchCourses(query) {
    if(!query) return
    // this.CourseService.searchCourses(query).then(courses => {
    //   this.courses = courses
    // })
    console.log(query);
  }

  cancelSearch() {
    this.onCancel()   
  }
}

SearchController.$inject = ['CourseService', '$window', '$timeout']