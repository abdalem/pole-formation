export default function routes($stateProvider) {
  $stateProvider
    .state('app.courses', {
      url: '/cours',
      component: 'list'
    })
    .state('app.course', {
      url: '/cours/:id',
      component: 'course'
    })
}

routes.$inject = ['$stateProvider']
