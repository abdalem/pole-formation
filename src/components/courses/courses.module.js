import routes from './courses.routes'
import listComponent from './list/list.component'
import courseComponent from './course/course.component'

export default angular.module('pf.courses', [])
  .config(routes)
  .component('list', listComponent)
  .component('course', courseComponent)