export default class ListController {
  constructor($mdMedia, CourseService) {
    this.$mdMedia = $mdMedia
    this.CourseService = CourseService

    this.categories = [
      {name: "Design", id: 1},
      {name: "Initiation numérique", id: 1},
      {name: "Marketing Digital", id: 2},
      {name: "Développement mobile", id: 3},
      {name: "Développement web", id: 4},
      {name: "Gestion de projet", id: 4},
      {name: "Entrepreneuriat", id: 6},
      {name: "Systèmes et réseaux", id: 7}
    ]
  }

  $onInit () {
  }
}

ListController.$inject = ['$mdMedia', 'CourseService']