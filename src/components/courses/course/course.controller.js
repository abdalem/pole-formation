export default class CourseController {
  constructor($mdMedia, CourseService, $stateParams) {
    this.$mdMedia = $mdMedia
    this.CourseService = CourseService
    this.id = $stateParams.id
  }

  $onInit () {
    this.CourseService.getCourse(this.id).then(course => {
      this.course = course
      this.course.duration = this.course.duration.match(/\d+/g).map(Number)[0]
    })

    this.CourseService.getTableOfContent(this.id).then(tableOfContent => {
      this.tableOfContent = tableOfContent
      console.log(tableOfContent.originalElement);
    })

    this.CourseService.getIntroduction(this.id).then(introduction => {
      this.introduction = introduction
    })
  }

  openSubMenu (index) {
    this.open = this.selected === index ? !this.open : true
    this.selected = index
  }

  getStyle(elem, count, index) {
    let style = {'max-height': null}

    if (elem === 'tof') {
      style['max-height'] = ((this.tableOfContent.children.length + 2) * 44)
      if(this.open) {
        style['max-height'] += (this.tableOfContent.children[this.selected].children.length + 1) * 36
      }
    } else if (elem === 'lessons' && this.open && this.selected === index) {
      style['max-height'] = (count + 1) * 36
    }

    style['max-height'] += 'px'
    return style
  }
}

CourseController.$inject = ['$mdMedia', 'CourseService', '$stateParams']