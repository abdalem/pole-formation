import template from './progressBar.html'
import controller from './progressBar.controller'

export default {
  template,
  controller,
  bindings: {
    progress: '<',
    score: '<',
    disabled: '<'
  }
}
