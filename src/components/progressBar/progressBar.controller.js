export default class progressBarController {
  constructor(CourseService, $window, $timeout) {
    this.CourseService = CourseService
    this.$window = $window
    this.$timeout = $timeout
  }

  searchCourses(query) {
    if(!query) return
    // this.CourseService.searchCourses(query).then(courses => {
    //   this.courses = courses
    // })
    console.log(query);
  }

  cancelSearch() {
    this.onCancel()   
  }
}

progressBarController.$inject = ['CourseService', '$window', '$timeout']