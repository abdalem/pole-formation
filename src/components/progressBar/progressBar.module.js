import progressBarComponent from './progressBar.component'

export default angular.module('pf.progressBar', [])
  .component('progressBar', progressBarComponent)