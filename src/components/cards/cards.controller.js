export default class CardsController {
  constructor(CourseService, $window, $timeout) {
    this.CourseService = CourseService
    this.$window = $window
    this.$timeout = $timeout

    this.layout = "row"
    this.layoutAlign = "start start"
    this.layoutWrap = true
  }

  $onInit() {
    angular.element(document.querySelector("cards")).addClass("layout-wrap layout-row layout-align-start-start")
  }

  searchCourses(query) {
    if(!query) return
    // this.CourseService.searchCourses(query).then(courses => {
    //   this.courses = courses
    // })
    console.log(query);
  }

  cancelSearch() {
    this.onCancel()   
  }
}

CardsController.$inject = ['CourseService', '$window', '$timeout']