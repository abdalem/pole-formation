import cardsComponent from './cards.component'

export default angular.module('pf.cards', [])
  .component('cards', cardsComponent)