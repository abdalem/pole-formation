import template from './cards.html'
import controller from './cards.controller'

export default {
  template,
  controller,
  bindings: {
    element: '<',
    type: '<',
    layout: '<',
    layoutAlign: '<',
    layoutWrap: '<'
  }
}
