export default class AuthenticateController {
    constructor($mdMedia, $mdToast, $state, $rootScope, AuthService, UserService) {
        this.$mdMedia = $mdMedia
        this.$mdToast = $mdToast
        this.$state = $state
        this.$rootScope = $rootScope
        this.AuthService = AuthService
        this.UserService = UserService

        this.patterns = {
            email: /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9-]+\.[a-zA-Z]{1,5}$/,
        }
        this.authToast = {
            success: {content: "Heureux de vous revoir !", class: "md-green"},
            error: {content: "Veuillez vérifier vos informations de connexion !", class: "md-red"}
        }
    }

    $onInit() {
//      this.AuthService.authorizationRequest();

    }

    authenticate(user) {
        user.email = user.email.toLowerCase()
        this.AuthService.tokenRequest(user.email, user.password).success(response => {
            this.$mdToast.show(
                    this.$mdToast.simple()
                    .textContent(this.authToast.success.content)
                    .position('top center')
                    .hideDelay(2000)
                    .toastClass("success md-larger")
                    )
            this.$state.go('app.dashboard')
        }).error(response => {
            this.$mdToast.show(
                    this.$mdToast.simple()
                    .textContent(this.authToast.error.content)
                    .position('top center')
                    .hideDelay(2000)
                    .toastClass("error md-larger")
                    )
        });
    }
}

AuthenticateController.$inject = ['$mdMedia', '$mdToast', '$state', '$rootScope', 'AuthService', 'UserService']
