import template from './authenticate.html'
import controller from './authenticate.controller'

export default {
  template,
  controller,
  bindings: {
    layout: '@',
    layoutFill: '@',
    layoutAlign: '@'
  }
}
