import routes from './authenticate.routes'
import authenticateComponent from './authenticate.component'

export default angular.module('pf.authenticate', [])
  .config(routes)
  .component('authenticate', authenticateComponent)