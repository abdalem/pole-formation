export default function routes($stateProvider) {
  $stateProvider
    .state('authenticate', {
      url: '/connexion',
      component: 'authenticate',
      resolve: {
        layout: () => 'column',
        layoutFill: () => true,
        layoutAlign: () => 'center center'
      }
    })
}

routes.$inject = ['$stateProvider']
