export default function routes($stateProvider) {
  $stateProvider
    .state('app.dashboard', {
      url: '/tableau-de-bord',
      component: 'dashboard'
    })
}

routes.$inject = ['$stateProvider']
