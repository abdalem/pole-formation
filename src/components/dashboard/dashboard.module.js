import routes from './dashboard.routes'
import dashboardComponent from './dashboard.component'

export default angular.module('pf.dashboard', [])
  .config(routes)
  .component('dashboard', dashboardComponent)