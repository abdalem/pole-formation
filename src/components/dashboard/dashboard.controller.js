export default class DashboardController {
  constructor($mdMedia, UserService) {
    this.$mdMedia = $mdMedia
    this.UserService = UserService

    // Récuperer les cours de l'utilisateurs

    // this.courses = user.courses
  }

  $onInit () {
    this.UserService.getCurrentUser().then(user => {
      this.user = user
      console.log(user);

      this.UserService.getFollowedCourses(user.id).then(courses => {
        this.courses = courses

        console.log(courses);
        
      })
    })
  }
}

DashboardController.$inject = ['$mdMedia', 'UserService']