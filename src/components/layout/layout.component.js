import template from './layout.html'
import controller from './layout.controller'

export default {
  template,
  controller,
  bindings: {
    layout: '@',
    layoutFill: '@',
    layoutAlign: '@'
  }
}
