export default class LayoutController {
  constructor($mdMedia, $mdDialog, AuthService, $state) {
    this.$mdMedia = $mdMedia
    this.$mdDialog = $mdDialog
    this.AuthService = AuthService
    this.$state = $state

    this.menuItems = [
      {name: "Tableau de bord", uiParams: "app.dashboard"},
      {name: "Cours", uiParams: "app.courses"}
    ]
  }

  unauthenticate() {
    console.log("here");
    
    this.$state.go('authenticate')
  }

  updateSearchParams() {
    this.search = !this.search
  }
}

  LayoutController.$inject = ['$mdMedia', '$mdDialog', 'AuthService', '$state']