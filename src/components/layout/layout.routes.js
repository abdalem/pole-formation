export default function routes($stateProvider) {
  $stateProvider
    .state('app', {
      component: 'layout',
      resolve: {
        layout: () => 'column',
        layoutFill: () => true,
        layoutAlign: () => 'start stretch'
      }
    })
}

routes.$inject = ['$stateProvider']
