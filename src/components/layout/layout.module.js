import routes from './layout.routes'
import layoutComponent from './layout.component'

export default angular.module('pf.layout', [])
  .config(routes)
  .component('layout', layoutComponent)