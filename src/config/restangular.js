export default function restangularConfig(RestangularProvider, $windowProvider) {

//    RestangularProvider.setBaseUrl('https://api.openclassrooms.com');
// Only for development
    RestangularProvider.setBaseUrl('https://private-anon-67f60270d8-openclassrooms.apiary-mock.com');
    var headers = {}
    if (typeof $windowProvider.$get().localStorage.getItem('access_token') !== 'undefined') {
        headers = {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
            'Authorization': 'Bearer ' + $windowProvider.$get().localStorage.getItem('access_token')
        }
    } else {
        headers = {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*'
        }
    }
    RestangularProvider.setDefaultHeaders(headers);
    RestangularProvider.setPlainByDefault = true;

    RestangularProvider.setResponseExtractor(function (response) {
        var newResponse = response;
        if(angular.isObject(response)) {
            newResponse.originalElement = angular.copy(response);
        }
        return newResponse;
    });
}

restangularConfig.$inject = ['RestangularProvider', '$windowProvider'];
