export default function theming($mdIconProvider, $mdThemingProvider, $mdDateLocaleProvider) {
   $mdIconProvider
        .icon("close", "./assets/icons/close.svg", 24)
        .icon("power", "./assets/icons/power.svg", 24)
        .icon("search", "./assets/icons/search.svg", 24)
        .icon("difficulty", "./assets/icons/difficulty.svg", 24)
        .icon("duration", "./assets/icons/duration.svg", 24)
        .icon("arrow", "./assets/icons/arrow.svg", 24)
        .icon("menu", "./assets/icons/menu.svg", 24)
        .icon("pin", "./assets/icons/pin.svg", 24)
        .icon("progress", "./assets/icons/progress.svg", 24)

    $mdDateLocaleProvider.formatDate = function (date) {
        return moment(date, 'DD/MM/YYYY');
    };

    $mdThemingProvider.definePalette('pf-blue', {
        '50': 'e5e6ed',
        '100': 'bfc1d3',
        '200': '9497b5',
        '300': '696d97',
        '400': '484e81',
        '500': '282f6b',
        '600': '242a63',
        '700': '1e2358',
        '800': '181d4e',
        '900': '0f123c',
        'A100': '777eff',
        'A200': '444eff',
        'A400': '111eff',
        'A700': '000df6',
        'contrastDefaultColor': 'light',
        'contrastDarkColors': [
            '50',
            '100',
            '200',
            'A100'
        ],
        'contrastLightColors': [
            '300',
            '400',
            '500',
            '600',
            '700',
            '800',
            '900',
            'A200',
            'A400',
            'A700'
        ]
    });

    $mdThemingProvider.definePalette('pf-blue-2', {
        '50': 'edf4fa',
        '100': 'd1e3f2',
        '200': 'b3d0e9',
        '300': '94bde0',
        '400': '7dafd9',
        '500': '66a1d2',
        '600': '5e99cd',
        '700': '538fc7',
        '800': '4985c1',
        '900': '3874b6',
        'A100': 'ffffff',
        'A200': 'cee4ff',
        'A400': '9bc9ff',
        'A700': '81bbff',
        'contrastDefaultColor': 'light',
        'contrastDarkColors': [
            '50',
            '100',
            '200',
            '300',
            '400',
            '500',
            '600',
            '700',
            'A100',
            'A200',
            'A400',
            'A700'
        ],
        'contrastLightColors': [
            '800',
            '900'
        ]
    });

    $mdThemingProvider.theme('default')
            .primaryPalette('pf-blue')
            .accentPalette('pf-blue-2');

//    Ne pas oublier de mettre en place le sprite.dist.svg pour les icones
    //$mdIconProvider.defaultIconSet('/assets/icons/sprite.svg');
}

theming.$inject = ['$mdIconProvider', '$mdThemingProvider', '$mdDateLocaleProvider']
