export default function routes($urlRouterProvider) {
  $urlRouterProvider.otherwise('/connexion')
}

routes.$inject = ['$urlRouterProvider']
