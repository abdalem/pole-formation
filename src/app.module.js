import '../node_modules/angular-material/angular-material.scss'
import './assets/app.scss'

import 'angular'
import '@uirouter/angularjs'
import 'angular-material'
import 'restangular'
import 'angular-sanitize'
import 'ng-videosharing-embed'

import './app.components'

import globalRoutes from './config/routes'
import theming from './config/theming'
import restangularConfig from './config/restangular'
import AuthService from './services/auth.service'
import UserService from './services/user.service'
import PathService from './services/path.service'
import ProjectService from './services/project.service'
import CourseService from './services/course.service'

angular.module('pf', [
    'ui.router',
    'ngMaterial',
    'ngSanitize',
    'restangular',
    'videosharing-embed',
    'pf.authenticate',
    'pf.layout',
    'pf.dashboard',
    'pf.search',
    'pf.courses',
    'pf.cards',
    'pf.progressBar'
  ])
  .config(globalRoutes)
  .config(restangularConfig)
  .config(theming)
  .service('AuthService', AuthService)
  .service('UserService', UserService)
  .service('PathService', PathService)
  .service('ProjectService', ProjectService)
  .service('CourseService', CourseService)
