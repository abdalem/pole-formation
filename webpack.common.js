const webpack = require('webpack')
const path = require('path')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const WriteFilePlugin = require('write-file-webpack-plugin')
const CleanWebpackPlugin = require('clean-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const SvgStorePlugin = require('external-svg-sprite-loader/lib/SvgStorePlugin')

module.exports = {
    entry: {
        'app': './src/app.module.js'
    },
    module: {
        rules: [
            {
                test: /^(?!.*\.{test,min}\.js$).*\.js$/,
                exclude: /node_modules/,
                use: 'babel-loader'
            },
            {
                test: /\.html$/,
                loader: "html-loader"
            },
            {
                test: /\.scss$/,
                use: [
                    {loader: 'style-loader'},
                    {loader: 'css-loader'},
                    {loader: 'sass-loader'}
                ]
            },
            {
                test: /\.(jpe?g|png|gif|svg)$/,
                exclude: /src\/assets\/icons/,
                use: [{
                        loader: 'file-loader',
                        options: {
                            name: 'images/[name].[ext]'
                        }
                    }]
            },
            {
                test: /\.(eot|woff|woff2|svg|ttf|otf)([\?]?.*)$/,
                exclude: /src\/assets\/icons/,
                use: [{
                        loader: 'file-loader',
                        options: {
                            name: 'fonts/[name].[ext]'
                        }
                    }]
            },
            {
                test: /\.svg$/,
                include: [path.resolve(__dirname, 'src/assets/icons')],
                loader: 'external-svg-sprite-loader',
                options: {
//                    name: '/dist/assets/icons/sprite.svg',
                    iconName: '[name]'
                }
            }
        ]
    },
    plugins: [
        new CleanWebpackPlugin(['dist']),
        new WriteFilePlugin(),
        new CopyWebpackPlugin([
            {from: 'src/assets/img', to: 'assets/img'},
//            {from: 'src/assets/icons', to: 'assets/icons'},
            {from: 'src/assets/fonts', to: 'assets/fonts'}
        ]),
        new HtmlWebpackPlugin({
            template: './src/index.html'
        }),
        new SvgStorePlugin()
    ],
    optimization: {
        splitChunks: {
            cacheGroups: {
                default: false,
                commons: {
                    test: /[\\/]node_modules[\\/]/,
                    name: "vendors",
                    chunks: "all"
                }
            }
        }
    },
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'libs/[name].bundle.js',
        chunkFilename: 'libs/[name]-chunk.js'
    }
}