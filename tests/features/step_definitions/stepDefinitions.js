var chai = require('chai');
var chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);

var expect = chai.expect;

module.exports = function() {    
    this.Given(/^I go to "([^"]*)"$/, function (url, next) {
        browser.get(url);
        next();
    });

    this.When(/^I insert text "([^"]*)" in login input$/, function (login, next) {
        element(by.id('login')).sendKeys(login);
        next();
    });

    this.When(/^I insert text "([^"]*)" in password input$/, function (password, next) {
        element(by.id('password')).sendKeys(password);
        next();
    });

    this.When(/^I click button "([^"]*)"$/, function (button, next) {
        element(by.id('connexion')).click();
        next();
    });
    
    this.When(/^I insert text "([^"]*)" in search input$/, function (device, next) {
        element(by.id('search-input')).sendKeys(device);
        var enter = browser.actions().sendKeys(protractor.Key.ENTER);
        enter.perform();
        next();
    });

    this.Then(/^I should be able to see all devices like "([^"]*)"$/, function (device, next) {
        browser.waitForAngular();
        element.all(by.css('.md-3-line')).then(function(devices) {
            expect(devices[0].element(by.css('h3')).getText()).to.contain(device);
        })
        next();
    });

    this.Then(/^I should be able go to "([^"]*)"$/, function (url, next) {
        browser.get(url);
        browser.waitForAngular();
        expect(browser.getCurrentUrl()).to.eventually.contain('devices').and.notify(next);
    });
};
