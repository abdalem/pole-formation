Feature: Connect to administration
    As a user of Avantages
    I should be able to connect to admin
    In order to administrate datas

    Scenario: Connect to administration
        Given I go to "http://localhost/index-dev.html#/admin"
        When I insert text "admin" in login input
        And I insert text "admin" in password input
        And I click button "connexion"
        Then I should be able go to "http://localhost/index-dev.html#/admin"
